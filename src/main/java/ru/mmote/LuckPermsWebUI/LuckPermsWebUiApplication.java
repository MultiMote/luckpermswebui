package ru.mmote.LuckPermsWebUI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LuckPermsWebUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LuckPermsWebUiApplication.class, args);
	}

}
