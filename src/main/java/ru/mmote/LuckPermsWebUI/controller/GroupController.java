package ru.mmote.LuckPermsWebUI.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ru.mmote.LuckPermsWebUI.repository.GroupRepository;

@Controller
@RequestMapping(value = "/groups")
public class GroupController {
    private final GroupRepository repo;

    public GroupController(GroupRepository repo) {
        this.repo = repo;
    }

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("items", repo.findAll());
        return "groups/list";
    }
}
