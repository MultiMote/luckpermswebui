package ru.mmote.LuckPermsWebUI.controller;

import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.mmote.LuckPermsWebUI.NotFoundException;
import ru.mmote.LuckPermsWebUI.entity.GroupPermission;
import ru.mmote.LuckPermsWebUI.repository.GroupPermissionRepository;
import ru.mmote.LuckPermsWebUI.repository.GroupRepository;

@Controller
@RequestMapping(value = "/group_permissions")
public class GroupPermissionController {

    private final GroupPermissionRepository groupPermsRepo;

    private final GroupRepository groupsRepo;

    public GroupPermissionController(GroupPermissionRepository groupPermsRepo, GroupRepository groupsRepo) {
        this.groupPermsRepo = groupPermsRepo;
        this.groupsRepo = groupsRepo;
    }

    @GetMapping
    public String getAll(Model model) {
        model.addAttribute("items", groupPermsRepo.findAll());
        return "group_permissions/list";
    }

    @GetMapping("/{id}")
    public String getAllByGroup(Model model, @PathVariable String id) {
        model.addAttribute("items", groupPermsRepo.findByGroup_name(id));
        return "group_permissions/list";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("item", new GroupPermission());
        model.addAttribute("groups", groupsRepo.findAll());
        return "group_permissions/edit";
    }

    @GetMapping("/edit/{id}")
    public String getEdit(@PathVariable Integer id, Model model) {
        model.addAttribute("groups", groupsRepo.findAll());
        model.addAttribute("item", groupPermsRepo.findById(id).orElseThrow(() -> new NotFoundException("Permission not found")));
        return "group_permissions/edit";
    }

    @GetMapping("/delete/{id}")
    public String getEdit(@PathVariable Integer id) {
        GroupPermission item = groupPermsRepo.findById(id).orElseThrow(() -> new NotFoundException("Permission not found"));
        groupPermsRepo.delete(item);
        return "redirect:/group_permissions" + item.getGroup().getName();
    }

    @PostMapping("/edit")
    public String postEdit(Model model, @Valid @ModelAttribute("item") GroupPermission item, BindingResult result) {
        if (result.hasErrors()) {
            model.addAttribute("groups", groupsRepo.findAll());
            return "group_permissions/edit";
        }
        groupPermsRepo.save(item);
        return "redirect:/group_permissions/" + item.getGroup().getName();
    }
}
