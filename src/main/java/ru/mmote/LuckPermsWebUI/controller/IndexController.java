package ru.mmote.LuckPermsWebUI.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {
    @GetMapping("/")
    public String getAll(Model model){
        return "redirect:/group_permissions";
    }
}