package ru.mmote.LuckPermsWebUI.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "luckperms_groups")
public class Group {
    @Id
    private String name;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL)
    private List<GroupPermission> permissions;

    public Group() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GroupPermission> getPermissions() {
        return permissions;
    }

}