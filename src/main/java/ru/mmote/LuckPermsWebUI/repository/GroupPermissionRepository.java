package ru.mmote.LuckPermsWebUI.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.mmote.LuckPermsWebUI.entity.GroupPermission;

public interface GroupPermissionRepository extends JpaRepository<GroupPermission, Integer> {
    List<GroupPermission> findByGroup_name(String name);
}