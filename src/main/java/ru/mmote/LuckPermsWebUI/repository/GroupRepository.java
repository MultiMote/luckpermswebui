package ru.mmote.LuckPermsWebUI.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ru.mmote.LuckPermsWebUI.entity.Group;

public interface GroupRepository extends JpaRepository<Group, String> {

}